﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackEffect : MonoBehaviour
{
    public GameObject KnifeEffect;
    public AudioClip KnifeSound;

    public GameObject GunShootEffect;
    public AudioClip GunSound;

    private AudioSource source;


    void Start()
    {
        source = GetComponent<AudioSource>();
        GunShootEffect.SetActive(false);
        KnifeEffect.SetActive(false);
    }
    public void KnifeAttackStart()
    {
        source.PlayOneShot(KnifeSound);
        KnifeEffect.SetActive(true);
    }
    public void KnifeAttackEnd()
    {
        KnifeEffect.SetActive(false);
    }
    public void GunShootStart()
    {
        GunShootEffect.SetActive(false);
        source.PlayOneShot(GunSound);
        //GunShootEffect.SetActive(true);
    }
    public void GunShootEnd()
    {
        GunShootEffect.SetActive(false);
    }
}
