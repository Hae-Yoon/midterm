﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeAttack : MonoBehaviour {

    public bool hit = false;
    public GameObject enemy;
    public GameObject parent;

    private void Start()
    {
        parent = transform.root.GetComponent<GameObject>();
    }
    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "ENEMY")
        {
            hit = true;
            enemy = coll.gameObject;
        }
    }
}
