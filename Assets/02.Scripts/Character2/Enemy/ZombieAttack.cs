﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAttack : MonoBehaviour {

    public bool hit = false;
    public GameObject player;


    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PLAYER")
        {
            hit = true;
            player = coll.gameObject;
        }
    }
}
