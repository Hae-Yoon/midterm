﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Enemy : Character 
{
    //distance between player and enemy
    public float distance;

    //variables for aggro
    public bool aggroFlag = false;
    private float currentAggro;
    public float AggroRange = 5.0f;
    public float aggroPassRange = 5.0f;

    public float attackDelay = 1.19f;
    private float currTime = 0;
    private bool Dead = false;
    private UnityEngine.AI.NavMeshAgent navi;
    public Collider colli;

    public SphereCollider AttackHand;
    public ZombieAttack Zomatt;

    public GameObject marker;
    // Use this for initialization
    void Start()
    {
        FullHP = HP;
        currentAggro = AggroRange;

        AttackHand.enabled = false;
        navi = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
        myTr = GetComponent<Transform>();
        state = new StateMgr[5] { idle, walk, attack, die, wait };
        Target = GameObject.FindGameObjectWithTag("PLAYER");
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(myTr.position, Target.transform.position);
        state[(int)charState]();
        if (HP>0)
        {
            AI();
            //damaged 
            if (isHit)
            {
                if (HP <= 0)
                    charState = Character.State.die;
                else 
                    charState = Character.State.wait;
                Color toColor = Color.red;
                float lerp = Mathf.PingPong(Time.time, 0.1f) / 0.1f;
                rend.material.color = Color.Lerp(originColor, toColor, lerp);
                
            }
        }

		base.Update();
    }
    public void AI()
    {
        if (distance <= AggroRange)
        {
            aggroFlag = true;

            if (distance <= attackRange_short && charState != State.attack)
                charState = State.attack;
        }
        else
            aggroFlag = false;

        if(aggroFlag)
        {
            if (charState == State.idle)
                charState = State.walk;
        }

        if(GameManager.instance.gmState == GameManager.GameState.clear)
        {
            charState = State.die;
        }
    }

    //AI for Enemys
    protected override void idle()
    {
        base.idle();
        navi.Stop();

        
    }

    //Enemy's walk is track player
    protected override void walk()
    {
        //get character's walk method
        base.walk();

        //new method for enemy walking
        myTr.forward = Target.transform.position - myTr.position;
        Vector3 targetPosition = (myTr.position - Target.transform.position); // get direction
        myTr.transform.Translate(targetPosition.normalized * moveSpeed * Time.deltaTime);//make movement

        //change walking method to NavMeshAgent.
        navi.SetDestination(Target.transform.position);
        navi.isStopped = false;
    }

    protected override void attack()
    {
        base.attack();
        currTime += Time.deltaTime;
        if (currTime >= attackDelay)
        {
            currTime = 0;
            anim.SetBool("IsWaiting", true);
            charState = Character.State.wait;
        }

    }

    void wait()
    {
        currTime += Time.deltaTime;
        if (Dead)
        {
            if (currTime >= 2f)
            {
                anim.enabled = false;
                this.enabled = false;
            }
        }
        else if ((!isHit && currTime >= 2f) || (isHit && currTime >= 1f))
        {
            anim.SetBool("IsWaiting", false);
            currTime = 0;
            if (distance > currentAggro)
            {
                charState = Character.State.idle;
                aggroFlag = false;
            }
            else if (distance > attackRange_short)
                charState = Character.State.walk;
            else
                charState = Character.State.attack;
        }
        

    }

    public void GiveDamage()
    {
        AttackHand.enabled = true;
        if (distance <= attackRange_short && Zomatt.hit)
        {
            Zomatt.player.GetComponent<Player>().OnDamaged(1);
            Zomatt.hit = false;
        }
        else
            return;
    }
    public void EndAttack()
    {
        AttackHand.enabled = false;
    }

    protected override void die()
    {
        navi.enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        colli.enabled = false;
        marker.SetActive(false);
        Dead = true;
        base.die();
        GameManager.instance.killPoint++;
        this.enabled = false;
    }

    private void DeadProcess()
    {
        navi.Stop();
        navi.enabled = false;
        charState = Character.State.die;
        
    }

    public void GetAggro(GameObject target)
    {
        Target = target;
        aggroFlag = true;

    }
}
