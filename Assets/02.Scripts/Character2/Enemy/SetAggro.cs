﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAggro : MonoBehaviour {

    public void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "PLAYER")
        {
            GetComponentInParent<Enemy>().GetAggro(coll.gameObject);
        }

        //if coll at enemy aggro collider, get aggro from that.
        Enemy enemyScript = coll.gameObject.GetComponent<Enemy>();
        if (coll.tag == "ENEMY"
            && enemyScript.aggroFlag == false
            && enemyScript.charState == Character.State.idle
            && enemyScript.charState != Character.State.idle
            && enemyScript.aggroFlag == true)

            enemyScript.aggroFlag = true;

    }
}
