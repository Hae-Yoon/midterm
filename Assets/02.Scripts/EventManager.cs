﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    bool clearTrigger = false;
    public GameObject rejectEvent;
    public GameObject clearEvent;

    private void Start()
    {
        clearEvent.SetActive(false);
        rejectEvent.SetActive(false);
    }
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "PLAYER")
        {
            if(GameManager.instance.haveOil)
            {
                clearEvent.SetActive(true);
            }
            else
            {
                rejectEvent.SetActive(true);
            }
        }
        
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "PLAYER")
        {
            if (clearEvent)
            {
                if (Input.GetKeyDown(KeyCode.P))
                {
                    clearEvent.SetActive(false);
                    GameManager.instance.gmState = GameManager.GameState.clear;
                }
            }
            
        }
    }
    public void OnTriggerExit(Collider other)
    {
        clearEvent.SetActive(false);
        
        rejectEvent.SetActive(false);
        
    }
}
