﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool haveOil = false;
    private bool alreadyPlayClear = false;
    public int killPoint;
    public float playTime;

    private float checkTime =0 ;
    public GameObject pausedScreen;
    public GameObject clearScreen;
    public GameObject gameOverScreen;

    public GameObject clearMark;
    public GameObject killText;
    public GameObject timeText;
    public GameObject[] Star;

    public enum GameState { playing, paused, clear, gameOver };
    public GameState gmState = GameState.playing;
    private delegate void StateMgr();
    private StateMgr[] state;
    private float currTime = 0;

    private AudioSource audioPlay;
    public AudioClip basicBgm;
    public AudioClip clearSound;
    public AudioClip gameOverSound;

    public static GameManager instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Use this for initialization
    void Start()
    {
        pausedScreen.SetActive(false);
        clearScreen.SetActive(false);
        gameOverScreen.SetActive(false);
        state = new StateMgr[4] { playing, paused, clear, gameOver };
        audioPlay = GetComponent<AudioSource>();
        audioPlay.Play();
    }

    // Update is called once per frame
    void Update()
    {
        state[(int)gmState]();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gmState == GameState.playing)
                gmState = GameState.paused;
            else if (gmState == GameState.paused)
                gmState = GameState.playing;
        }
    }

    void playing()
    {
        Time.timeScale = 1;
        pausedScreen.SetActive(false);
        audioPlay.volume = 1f;
    }
    void paused()
    {
        audioPlay.volume = 0.3f;
        pausedScreen.SetActive(true);
        Time.timeScale = 0;
    }
    void clear()
    {
        clearScreen.SetActive(true);
        
        if (currTime < 2f)
        {
            currTime += Time.deltaTime;
            clearMark.GetComponent<Image>().color = new Color(1, 1, 1, currTime / 2);
        }
        else
            clearMark.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        if (!alreadyPlayClear)
            StartCoroutine(ClearProcess());
    }
    IEnumerator ClearProcess()
    {
        alreadyPlayClear = true;
        audioPlay.Stop();
        audioPlay.PlayOneShot(clearSound);
        playTime = Time.time;
        string timeStr = "" + playTime.ToString("00:00.00");

        timeText.GetComponent<Text>().text = timeStr;
        killText.GetComponent<Text>().text = killPoint.ToString();

        //score rating
        int score = 0;
        if (killPoint < 6)
            score += 1;
        else if (killPoint < 12)
            score += 2;
        else
            score += 3;

        if (playTime < 60)
            score += 3;
        else if (playTime < 120)
            score += 2;
        else
            score += 1;

        yield return new WaitForSeconds(2.0f);

        if (score > 5)
        {
            for (int i = 0; i < Star.Length; i++)
            {
                Star[i].SetActive(true);
                Star[i].GetComponent<Animation>().Play();
                yield return new WaitForSeconds(0.5f);
            }
        }
        else if (score > 3)
        {
            for (int i = 0; i < (Star.Length - 1); i++)
            {
                Star[i].SetActive(true);
                Star[i].GetComponent<Animation>().Play();
                yield return new WaitForSeconds(0.5f);
            }
        }
        else
        {
            Star[0].SetActive(true);
            Star[0].GetComponent<Animation>().Play();
        }


    }


    void gameOver()
    {
        audioPlay.Stop();
        audioPlay.PlayOneShot(gameOverSound);
        gameOverScreen.SetActive(true);
        Time.timeScale = 0;
    }
}
