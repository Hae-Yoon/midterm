﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityStandardAssets.Utility
{
	public class SmoothFollow : MonoBehaviour
	{
        private Transform camTr;
        private bool isCam_UserHit = false;
        private bool isMultiHit = false;
        private int hitNum=0;
        private GameObject hitObj = null;
        private GameObject[] hitObjs = new GameObject[5];

        // The target we are following
        [SerializeField]
		public Transform target;
		// The distance in the x-z plane to the target
		[SerializeField]
		public float distance = 10.0f;
		// the height we want the camera to be above the target
		[SerializeField]
		private float height = 5.0f;

		[SerializeField]
		private float rotationDamping;
		[SerializeField]
		private float heightDamping;

		// Use this for initialization
		void Start()
        {
            camTr = transform;
        }

        void Update()
        {
            CamToUserCheck();
            //Debug.Log("isHit : " + isCam_UserHit);
            //Debug.DrawRay(camTr.position, -(camTr.position - target.transform.position), Color.cyan);
            if (isMultiHit)
            {
                //Debug.Log("Mutihit : " + isMultiHit + " hitNum : " + hitNum);
                for (int i = 0; i <= hitNum; i++)
                    Debug.Log(hitObjs[i]);
            }
            else
            { }
               // Debug.Log(hitObj);
        }

		void LateUpdate()
		{
			// Early out if we don't have a target
			if (!target)
				return;

			// Calculate the current rotation angles
			var wantedRotationAngle = target.eulerAngles.y;
			var wantedHeight = target.position.y + height;

			var currentRotationAngle = transform.eulerAngles.y;
			var currentHeight = transform.position.y;

			// Damp the rotation around the y-axis
			currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

			// Damp the height
			currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

			// Convert the angle into a rotation
			var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

			// Set the position of the camera on the x-z plane to:
			// distance meters behind the target
			transform.position = target.position;
			transform.position -= currentRotation * Vector3.forward * distance;

			// Set the height of the camera
			transform.position = new Vector3(transform.position.x ,currentHeight , transform.position.z);

			// Always look at the target
			transform.LookAt(target);
        }

        private void CamToUserCheck()
        {
            RaycastHit wallHit = new RaycastHit();
            RaycastHit[] hits;
            
            Ray ray = new Ray(camTr.position, -(camTr.position - target.transform.position));
            
            if (Physics.Raycast(ray, out wallHit, Vector3.Distance(camTr.position, target.transform.position), 1 << 10))
            {
                isCam_UserHit = true;
                hits = Physics.RaycastAll(ray, Vector3.Distance(camTr.position, target.transform.position));
                if (hits.Length > 1)
                {
                    hitNum = hits.Length;
                    isMultiHit = true;
                    for (int i = 0; i < hitNum; i++)
                        hitObjs[i] = hits[i].transform.gameObject;
                }
                else
                {
                    hitObj = wallHit.transform.gameObject;
                }
                
                ChangeHitMat();
            }

            else if (hitObj != null)
            {
                isCam_UserHit = false;
                if (isMultiHit)
                {
                    isMultiHit = false;
                    for (int i = 0; i < hitNum; i++)
                    {
                        hitObjs[i].GetComponent<ChangeMaterial>().TpToBasic();
                        hitObjs[i] = null;
                    }
                    hitNum = 0;
                }
                else
                {
                    hitObj.GetComponent<ChangeMaterial>().TpToBasic();
                    hitObj = null;
                }
            }
        }

        private void ChangeHitMat()
        {
            if (isMultiHit)
            {
                for (int i = 0; i < hitNum; i++)
                {
                    hitObjs[i].GetComponent<ChangeMaterial>().BasicToTp();
                }
            }
            else
                hitObj.GetComponent<ChangeMaterial>().BasicToTp();
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "FIELD_OBJ")
            {
                if (isCam_UserHit)
                {
                    if (!isMultiHit)
                    {
                        isMultiHit = true;
                        hitObjs[0] = hitObj;
                        hitObj = null;
                        hitNum = 1;
                    }
                    hitNum++;
                    hitObjs[hitNum] = coll.gameObject;
                }
                else
                {
                    isCam_UserHit = true;
                    hitObj = coll.gameObject;
                }

                ChangeHitMat();
            }
        }
    }
}