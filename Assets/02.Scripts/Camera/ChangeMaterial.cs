﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour
{
    public Material basicMat;
    public Material transparentMat;

    public MeshRenderer mrenderer;

    void Start()
    {
        mrenderer = GetComponent<MeshRenderer>();
        basicMat = mrenderer.material;
    }

    public void BasicToTp()
    {
        mrenderer.material = transparentMat;
    }

    public void TpToBasic()
    {
        mrenderer.material = basicMat;
    }

}
