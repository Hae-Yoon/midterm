﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    public void LoadGameButton()
    {
        SceneManager.LoadScene(1);
        SceneManager.LoadScene(2, LoadSceneMode.Additive);
    }

    
    public void PausedButton()
    {
        Time.timeScale = 1f;
        GameManager.instance.gmState = GameManager.GameState.playing;
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(0);
    }
}
